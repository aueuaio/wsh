# TODOs for wsh

## Features

- signal handling

- expansion of "..", ".", "../", "./", "~"

- [?] word expansion with wordexp

- tab completion: builtins, remote files, [...]

- [?] secure data exchange with TLS/SSL (openSSL)

- [?] non-interactive sessions


## Quality

- make a fixed number of auth attempts > 1

- coop exit on non-network related failures for HTTP client

- if address is already in use when starting server, wait and retry until a
  reasonable time has passed (4 mins? see https://stackoverflow.com/questions/5106674/error-address-already-in-use-while-binding-socket-with-address-but-the-port-num)

- have timeout for blocking read calls (there should be none?)

- handle HTTP reqs better, provide auth in http client, etc.
