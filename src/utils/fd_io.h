#ifndef FD_IO_H
#define FD_IO_H

#include <unistd.h> // ssize_t

int set_blocking_flag (int, int);

int read_chars (int, ssize_t, char*);
int write_chars (int, ssize_t, const char*);


#endif // FD_IO_H
