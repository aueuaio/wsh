// Make sigaction etc. available.
#define _POSIX_C_SOURCE 200809L

#include "fd_io.h"

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <unistd.h> // read, write, ssize_t
#include <errno.h> // errno
#include <assert.h> // assert
#include <signal.h> // sigaction, SIG*, struct sigaction, sigemptyset
#include <fcntl.h> // fcntl, FD_CLOEXEC


//TODO placeholder
#define EUEOF -1


int set_blocking_flag (int fd, int blocking)
{
  int flags = fcntl (fd, F_GETFL, 0);
  if (flags == -1)
      return -1;

  if (blocking)
      flags &= ~O_NONBLOCK;
  else
      flags |= O_NONBLOCK;
  return fcntl (fd, F_SETFL, flags);
}


int read_chars (int fd, ssize_t n, char* chars)
{
  ssize_t global_read = 0;
  ssize_t local_read  = 0;

  while (global_read < n)
  {
    do local_read = read (fd, chars + global_read, n - global_read);
    while (local_read == -1 && errno == EINTR);
    if (local_read == 0)
      return (errno = EUEOF, -1);
    else if (local_read == -1)
      return -1;

    global_read += local_read;
  }

  return 0;
}


int write_chars (int fd, ssize_t n, const char* chars)
{
  ssize_t          global_written = 0;
  ssize_t          local_written  = 0;
  int              outcome;
  struct sigaction old_sa;
  struct sigaction ign_sigpipe_sa;

  ign_sigpipe_sa.sa_handler = SIG_IGN;
  ign_sigpipe_sa.sa_flags   = 0;
  sigemptyset (&ign_sigpipe_sa.sa_mask);
  outcome = sigaction (SIGPIPE, &ign_sigpipe_sa, &old_sa);
  assert (outcome == 0);

  while (global_written < n)
  {
    do local_written = write (fd, chars + global_written, n - global_written);
    while (local_written == -1 && errno == EINTR);
    if (local_written == -1)
    {
      outcome = sigaction (SIGPIPE, &old_sa, NULL);
      assert (outcome == 0);
      return -1;
    }

    global_written += local_written;
  }

  outcome = sigaction (SIGPIPE, &old_sa, NULL);
  assert (outcome == 0);

  return 0;
}
