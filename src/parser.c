#include "parser.h"

#include <stdio.h> // printf
#include <stdlib.h> // malloc, free
#include <string.h> // strcmp, strncmp
#include <ctype.h> // isspace

#include "array_list.h" // *


// a parser written using these basic parsers should be written in a 'parser
// combinator' style: helper functions which parse single types of tokens at the
// start of a given string are combined to parse a whole command line by feeding
// the leftover of one parser to the next parser until the leftover is empty

int is_character_backslash_escaped (const char*, int);

// the caller should ensure the first char is not escaped by a backslash or
// is inside quotes; return 0 if any whitespace was consumed, -1 otherwise
int parse_whitespace ( const char*
                            , const char**
                          );

// alternatives is a NULL-terminated array of 0-terminated strings. The
// alternatives should be sorted so that alternatives that are strict substrings
// of other alternatives come after them, thus achieving the expected "greedy"
// behaviour; empty string alternatives always fail, so one can "mask out" an
// alternative by setting it to "". Return the index of the parsed alternative
// or -1 if no alternative matches. Each alternative is treated as being
// composed of special characters, and thus will only be parsed if matched
// exactly (without checking for any backslash escaping or quoting).
int parse_alternative ( const char**
                             , const char*
                             , const char**
                           );

// parse a whitespace-separated list of words and alloc them in *output (that
// will be a NULL-terminated array of 0-terminated strings); assumes the first
// char is unescaped, outside of any quotes. Returns 0 if successful (even if 0
// words where parsed), -1 if an internal malloc failed.
int parse_words ( int            (*) (char)
                      , const char*
                      , char* **
                      , const char**
                    );


int is_character_backslash_escaped (const char* s, int i)
{
  return (i > 0 && s[i-1] == '\\' && !is_character_backslash_escaped (s, i-1));
}


int parse_whitespace ( const char*  input
                            , const char** input_leftover
                            )
{
  int i;

  for (i = 0; isspace (input[i]) && input[i] != 0; i++);
  *input_leftover = &(input[i]);

  //printf ("[parse_ws]: input = %s, left = %s\n", input, *input_leftover);

  return i > 0 ? 0 : -1;
}


int parse_alternative ( const char** sorted_alternatives
                             , const char*  input
                             , const char** input_leftover
                             )
{
  int i;

  for (i = 0; sorted_alternatives[i] != NULL; i++)
  {
    if (sorted_alternatives[i][0] != 0)
    {
      int alternative_length;

      alternative_length = strlen (sorted_alternatives[i]);
      if (strncmp (sorted_alternatives[i], input, alternative_length) == 0)
      {
        *input_leftover = &(input[alternative_length]);
        break;
      }
    }
  }

  return sorted_alternatives[i] != NULL ? i : -1;
}


static const char* parse_word_recursive ( int (*is_word_char) (char)
                                  , int is_first_char_inside_quotes
                                  , int is_first_char_escaped
                                  , const char* input
                                  , char* output
                                  )
{
  if (*input == 0)
  {
    *output = 0;
    return input;
  }

  if (is_first_char_inside_quotes && *input != '"')
  {
    *output = *input;

    return
      parse_word_recursive ( is_word_char
                           , is_first_char_inside_quotes
                           , is_first_char_escaped
                           , input + 1
                           , output + 1
                           );
  }

  if (is_first_char_escaped)
  {
    is_first_char_escaped = 0;

    *output = *input;

    return
      parse_word_recursive ( is_word_char
                           , is_first_char_inside_quotes
                           , is_first_char_escaped
                           , input + 1
                           , output + 1
                           );
  }

  if (*input == '"')
  {
    is_first_char_inside_quotes = !is_first_char_inside_quotes;

    return parse_word_recursive ( is_word_char
                         , is_first_char_inside_quotes
                         , is_first_char_escaped
                         , input + 1
                         , output
                         );
  }

  if (*input == '\\')
  {
    is_first_char_escaped = 1;

    return parse_word_recursive ( is_word_char
                         , is_first_char_inside_quotes
                         , is_first_char_escaped
                         , input + 1
                         , output
                         );
  }

  if (is_word_char (*input))
  {
    *output = *input;

    return parse_word_recursive ( is_word_char
                         , is_first_char_inside_quotes
                         , is_first_char_escaped
                         , input + 1
                         , output + 1
                         );
  }
  else
  {
    *output = 0;
    return input;
  }
}


// parse characters until an unescaped non-word character is encountered and put
// them in *output. assumes the first character is outside quotes and unescaped.
// Return -1 if an internal malloc call failed; return 0 otherwise. In the
// latter case, *output will contain the parsed word or NULL if no characters
// were consumed; is_word_char is a function that must return true given a word
// char and false otherwise ('\' and '"' are handled separately so the return
// value of is_word_char on them doesn't matter).
static int parse_word ( int           (*is_word_char) (char)
                      , const char*   input
                      , char* *       output
                      , const char* * input_leftover
                      )
{
  *output = malloc (1 + strlen (input));
  if (*output == NULL)
    return -1;

  *input_leftover = parse_word_recursive (is_word_char, 0, 0, input, *output);

  printf ("[parser] parsed word: '%s'\n", *output);
  printf ("[parser] leftover: '%s'\n", *input_leftover);

  if (*input_leftover == input)
  {
    free (*output);
    *output = NULL;
  }

  //TODO realloc to actual size?

  return 0;
}


int parse_words ( int            (*is_word_char) (char)
                       , const char*    input
                       , char* **       output
                       , const char**   input_leftover
                       )
{
  char*              null_pointer = NULL;
  int                status;
  int                word_count;
  struct array_list* words;

  words = array_list_alloc ();
  if (words == NULL)
    return -1;
  status = array_list_init (words, sizeof **output, 4);
  if (status == -1)
    return (free (words), -1);

  *output = (char**) array_list_get_elements (words);

  word_count = 0;
  status = parse_whitespace (input, input_leftover);
  //TODO check err status?
  while (1)
  {
    status = array_list_push (words, (char*)&null_pointer);
    if (status == -1)
      return (array_list_free (words), -1);
    *output = (char**) array_list_get_elements (words);

    status = parse_word ( is_word_char
                        , *input_leftover
                        , (*output) + word_count
                        , input_leftover
                        );
    if (status == -1)
      return (array_list_free (words), -1);
    // i.e. no characters were consumed by 'parse_word': there is no next word
    if ((*output)[word_count] == NULL)
      break;

    word_count++;

    status = parse_whitespace (*input_leftover, input_leftover);
  }

  free (words);
  return 0;
}


static int is_word_char (char c)
{
  const char special_chars[] = " <>&|";

  for (int i = 0; special_chars[i] != 0; i++)
    if (c == special_chars[i])
      return 0;

  return 1;
}


static int parse_stdio_redirections ( const char*  input
                                    , char**       stdin_filename
                                    , char**       stdout_filename
                                    , char**       stderr_filename
                                    , const char** leftover
                                    )
{
  int status;

  *leftover = input;

  printf ("[parser] stdio reds input: '%s'\n", input);

  status = parse_whitespace (*leftover, leftover);

  status =
    parse_alternative ((const char*[]){ "<", NULL }, *leftover, leftover);
  if (status == 0)
  {
    status = parse_whitespace (*leftover, leftover);

    status =
      parse_word (is_word_char, *leftover, stdin_filename, leftover);
    if (*stdin_filename == NULL)
      return -2;
  }

  status = parse_whitespace (*leftover, leftover);

  status =
    parse_alternative ((const char*[]){ ">>", ">", NULL }, *leftover, leftover);
  if (status == 0)
  {
    status = parse_whitespace (*leftover, leftover);

    status =
      parse_word (is_word_char, *leftover, stderr_filename, leftover);
    if (*stderr_filename == NULL)
      return (free (*stdin_filename), -2);
  }
  else if (status == 1)
  {
    status = parse_whitespace (*leftover, leftover);

    status =
      parse_word (is_word_char, *leftover, stdout_filename, leftover);
    if (*stdout_filename == NULL)
      return (free (*stdin_filename), -2);

    status = parse_whitespace (*leftover, leftover);

    status =
      parse_alternative ((const char*[]){ ">>", NULL }, *leftover, leftover);
    if (status == 0)
    {
      status = parse_whitespace (*leftover, leftover);

      status =
        parse_word (is_word_char, *leftover, stderr_filename, leftover);
      if (*stderr_filename == NULL)
        return (free (*stdin_filename), free (*stdout_filename), -2);
    }
  }

  printf ("[parser] stdio reds leftover: '%s'\n", *leftover);

  return 0;
}

int parse_command_line ( const char* command_line
                       , int* is_command_foreground
                       , int* process_count
                       , int* * command_argcs
                       , char* *** command_argvs
                       , char* * stdin_filename
                       , char* * stdout_filename
                       , char* * stderr_filename
                       , const char* * parse_error_message
                       )
{
  void* const null_pointer = NULL;
  const int zero_int = 0;
  int status;
  const char* leftover = command_line;
  int argvs_count = 0;
  struct array_list* argcs_list;
  struct array_list* argvs_list;
  int*    argcs;
  char*** argvs;

  *parse_error_message = NULL;

  argcs_list = array_list_alloc ();
  if (argcs_list == NULL)
    return -1;

  status = array_list_init (argcs_list, sizeof *argcs, 2);
  if (status == -1)
    return (free (argcs_list), -1);

  argcs = (int*)array_list_get_elements (argcs_list);

  argvs_list = array_list_alloc ();
  if (argvs_list == NULL)
    return (array_list_free (argcs_list), -1);

  status = array_list_init (argvs_list, sizeof *argvs, 2);
  if (status == -1)
    return (array_list_free (argcs_list), free (argvs_list), -1);

  argvs = (char***)array_list_get_elements (argvs_list);

  argvs_count = 0;

  status = array_list_push (argcs_list, (char*)&zero_int);
  if (status == -1)
    return (array_list_free (argcs_list), array_list_free (argvs_list), -1);

  argcs = (int*)array_list_get_elements (argcs_list);

  status = array_list_push (argvs_list, (char*)&null_pointer);
  if (status == -1)
    return (array_list_free (argcs_list), array_list_free (argvs_list), -1);

  argvs = (char***)array_list_get_elements (argvs_list);

  while (1)
  {
    status = array_list_push (argcs_list, (char*)&zero_int);
    if (status == -1)
      return (array_list_free (argcs_list), array_list_free (argvs_list), -1);

    argcs = (int*)array_list_get_elements (argcs_list);

    status = array_list_push (argvs_list, (char*)&null_pointer);
    if (status == -1)
      return (array_list_free (argcs_list), array_list_free (argvs_list), -1);

    argvs = (char***)array_list_get_elements (argvs_list);

    status =
      parse_words (is_word_char, leftover, argvs + argvs_count, &leftover);
    if (status == -1)
      return (array_list_free (argcs_list), array_list_free (argvs_list), -1);
    // If either the command line contained no arguments or there are no
    // arguments after a pipe operator, the command line is invalid.
    if (argvs[argvs_count][0] == NULL)
    {
      *parse_error_message = "arguments expected";
      argvs_count++;
      goto free_argvs_argcs_and_fail;
    }

    for ( argcs[argvs_count] = 0;
          argvs[argvs_count][argcs[argvs_count]] != NULL;
          argcs[argvs_count]++
        );

    argvs_count++;

    status =
      parse_alternative ((const char*[]){ "|", NULL }, leftover, &leftover);
    if (status == -1)
      break;
  }

  status = parse_whitespace (leftover, &leftover);

  *stdin_filename = NULL;
  *stdout_filename = NULL;
  *stderr_filename = NULL;

  status = parse_stdio_redirections (leftover, stdin_filename, stdout_filename, stderr_filename, &leftover);
  if (status == -2)
  {
    *parse_error_message = "missing filename after redirection operator";
    goto free_argvs_argcs_and_fail;
  }

  printf ("[parser] stdin filename: '%s'\n", *stdin_filename);
  printf ("[parser] stdout filename: '%s'\n", *stdout_filename);
  printf ("[parser] stderr filename: '%s'\n", *stderr_filename);

  *process_count = argvs_count;

  *command_argcs = argcs;
  *command_argvs = argvs;

  printf ("[parser] parsed argvs (%i):\n", argvs_count);
  for (int i = 0; i < argvs_count; i++)
  {
    printf ("[parser] argv[%i]: ", i);
    for (int j = 0; j < argcs[i]; j++)
    {
      printf ("'%s', ", argvs[i][j]);
    }
    printf ("\n");
  }

  status =
    parse_alternative ((const char*[]){ "&", NULL }, leftover, &leftover);
  *is_command_foreground = (status == -1);

  status = parse_whitespace (leftover, &leftover);

  printf ("[parser] final leftover: '%s'\n", leftover);

  if (*leftover != 0)
  {
    *parse_error_message = "non empty remainder after parsing";
    goto free_stdio_filenames_etc_and_fail;
  }

  printf ("[parser] is_command_foreground: %i\n", *is_command_foreground);

  free (argcs_list);
  free (argvs_list);

  return 0;

free_stdio_filenames_etc_and_fail:
  if (*stdin_filename != NULL)
    free (*stdin_filename);
  if (*stdout_filename != NULL)
    free (*stdout_filename);
  if (*stderr_filename != NULL)
    free (*stderr_filename);
free_argvs_argcs_and_fail:
  for (int i = 0; i < argvs_count; i++)
  {
    for (int j = 0; argvs[i][j] != NULL; j++)
      free (argvs[i][j]);
    free (argvs[i]);
  }

  array_list_free (argcs_list);
  array_list_free (argvs_list);

  return -2;
}
