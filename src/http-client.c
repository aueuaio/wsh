// Make sigaction & co. and pselect available.
#define _POSIX_C_SOURCE 200809L

#include <stdio.h> // printf
#include <stdlib.h> // free, exit
#include <errno.h> // errno
#include <unistd.h>
#include <assert.h> // assert
#include <stdint.h> // uint16_t
#include <sys/select.h> // pselect, FD_*, fd_set
#include <sys/types.h> // connect, socket
#include <signal.h> // sigemptyset, sigset_t
#include <sys/socket.h> // connect, socket, AF_INET, SOCK_STREAM, PF_INET
#include <arpa/inet.h> // hton*, ntoh*, inet_addr, in_addr_t
#include <netinet/in.h> // sockaddr_in
#include <string.h> // strncmp, strerror, [...]
#include <strings.h> // strcasecmp
#include <ctype.h> // isspace
#include <stdarg.h>

#include "utils/fd_io.h" // write_chars
#include "msg_io.h" // enum msg_type, recv_msg, send_msg


// These globals are implicitely used by signal handlers,
// 'send_command_line_response' and 'main'; other uses are explicit (i.e. the
// address or the value of these variables is passed as an argument).
static int shell_server_socket;
static int is_reading_command_line = 0;


int main (int argc, char* argv[])
{
  int  connect_to_shell_server      (const char*, uint16_t);
  int  recv_and_process_msg         (int, int, int*);
  int  open_http_daemon_socket      (uint16_t);
  int  accept_and_serve_http_client (int, int, int*);

  int status;
  int http_daemon_socket, http_client_socket = -1;
  uint16_t http_port, shell_port;
  sigset_t empty_sigset;
  uint32_t msg_type;
  uint32_t msg_length;
  char* msg_payload;

  sigemptyset (&empty_sigset);

  // Parse cli args
  {
    if (argc != 3)
    {
      fprintf(stderr, "wrong number of args (2 expected)\n");
      exit (1);
    }

    {
      char* port_str;
      char* leftover;
      int   port;

      port_str = argv[2];

      errno = 0;
      port = strtol (port_str, &leftover, 0);
      if (errno == ERANGE || *leftover != 0 || (port > 65535 || port < 0))
      {
        fprintf (stderr, "the port argument does not represent a valid port number\n");
        exit (1);
      }
      http_port = port;
    }

    {
      char* port_str;
      char* leftover;
      int   port;

      port_str = argv[1];

      errno = 0;
      port = strtol (port_str, &leftover, 0);
      if (errno == ERANGE || *leftover != 0 || (port > 65535 || port < 0))
      {
        fprintf (stderr, "the port argument does not represent a valid port number\n");
        exit (1);
      }
      shell_port = port;
    }
  }

  shell_server_socket = connect_to_shell_server ("127.0.0.1", shell_port);

  status = recv_msg (shell_server_socket, &msg_type, &msg_length, &msg_payload);
  if (status < 0)
  {
    fprintf (stderr, "[http-wsh] failed to recv message\n");
    exit (1);
  }

  if (   msg_type != MSG_PAM_AUTHENTICATION_STATUS
      || (msg_type == MSG_PAM_AUTHENTICATION_STATUS && *msg_payload != 1)
     )
  {
    fprintf (stderr, "[http-wsh] expected authentication status message; are you running this client on the same machine as the shell server?\n");
    exit (1);
  }
  free (msg_payload);

  http_daemon_socket = open_http_daemon_socket (http_port);

  while (1)
  {
    fd_set shell_server_and_http_daemon_sockets;

    FD_ZERO (&shell_server_and_http_daemon_sockets);
    FD_SET (shell_server_socket, &shell_server_and_http_daemon_sockets);
    FD_SET (http_daemon_socket, &shell_server_and_http_daemon_sockets);

    // Atomically unblock all signals for the duration of the call to ensure
    // signals are only actually handled right after the call (and thus they do
    // not interfere with the main loop logic).
    do status = pselect ( FD_SETSIZE
                        , &shell_server_and_http_daemon_sockets
                        , NULL, NULL, NULL
                        , &empty_sigset
                        );
    while (status == -1 && errno == EINTR);
    if (status == -1)
    {
      //TODO tell server we outta here
      fprintf (stderr, "[cli] pselect failed\n");
      exit (1);
    }

    if (FD_ISSET (shell_server_socket, &shell_server_and_http_daemon_sockets))
    {
      http_client_socket = recv_and_process_msg (shell_server_socket, http_client_socket, &is_reading_command_line);
    }

    if ( FD_ISSET (http_daemon_socket, &shell_server_and_http_daemon_sockets)
         && is_reading_command_line
       )
    {
      http_client_socket = accept_and_serve_http_client (http_daemon_socket, shell_server_socket, &is_reading_command_line);
    }
  }

  assert ("it should not be possible to reach this statement" == NULL);
}


int accept_and_serve_http_client (int http_daemon_socket, int shell_server_socket, int* is_reading_command_line)
{
  int accept_client (int);
  int read_http_header_line (int, int*, char**);
  int parse_http_request_line (char*, char**, char**, char**);
  int read_http_request_header_fields (int);
  int send_http_bad_request_response (int, const char*);
  int send_http_not_implemented_response (int, const char*);
  int send_http_ok_response (int, int, const char*);

  int status;
  int http_client_socket;
  char* request_line;
  char* method;
  char* target;

  http_client_socket = accept_client (http_daemon_socket);

  status = read_http_header_line (http_client_socket, NULL, &request_line);
  if (status == -1)
  {
    fprintf (stderr, "[error] failed to read an http header line: %s\n", strerror (errno));
    exit (1);
  }

  status = parse_http_request_line (request_line, &method, &target, NULL);
  if (status == -1)
  {
    fprintf (stderr, "[BAD REQUEST] bad request line\n\n");
    send_http_bad_request_response (http_client_socket, "bad request line");
    close (http_client_socket);
    free (request_line);
    return -1;
  }

  printf ("[method] '%s'\n", method);
  printf ("[target] '%s'\n\n", target);

  if (strcasecmp ("POST", method) != 0)
  {
    fprintf (stderr, "[UNSUPPORTED REQUEST METHOD] %s\n\n", method);
    send_http_not_implemented_response (http_client_socket, "method not implemented");
    close (http_client_socket);
    free (request_line);
    return -1;
  }

  free (request_line);

  int content_length;
  char* content;

  content_length = read_http_request_header_fields (http_client_socket);
  if (content_length == -1)
  {
    fprintf (stderr, "[BAD REQUEST (client %i)] bad field line\n\n", http_client_socket);
    send_http_bad_request_response (http_client_socket, "bad field line");
    close (http_client_socket);
    return -1;
  }

  printf ("[content length] %i\n", content_length);

  //TODO support no content length field (see
  //https://stackoverflow.com/questions/11375507/getting-response-of-http-request-without-content-length)
  if (content_length <= 0)
  {
    fprintf (stderr, "[BAD REQUEST (client %i)] no content length\n\n", http_client_socket);
    send_http_bad_request_response (http_client_socket, "no content length");
    close (http_client_socket);
    return -1;
  }

  content = malloc (1 + content_length);
  read_chars (http_client_socket, content_length, content);
  content[content_length] = 0;
  printf ("[content] '%s'\n", content);

  *is_reading_command_line = 0;

  status = send_msg ( shell_server_socket
                    , MSG_COMMAND_LINE_RESPONSE
                    , 1 + strlen (content)
                    , content
                    );
  if (status < 0)
  {
    fprintf (stderr, "[wsh-httpd] failed to send command line\n");
    //TODO send reponse before exiting
    //TODO try to re-establish the connection with the shell server?
    exit (1);
  }

  //int command_output_length;
  //char* command_output;

  //recv_command_output (shell_server_socket, &command_output);

  //TODO wrap the pre html tags explicitely here instead of inside the
  //function
  //send_http_ok_response (http_client_socket, content_length, content);

  free (content);

  //close (http_client_socket);
  return http_client_socket;
}


int format_to_string (char** string_ptr, const char* format, ...)
{
  char* string;
  int string_length, status;
  va_list format_args;

  va_start (format_args, format);
  string_length = vsnprintf (NULL, 0, format, format_args);
  va_end (format_args);
  assert (string_length >= 0);

  string = malloc (1 + string_length);
  if (string == NULL)
    return -1;

  va_start (format_args, format);
  status = vsnprintf (string, 1 + string_length, format, format_args);
  va_end (format_args);
  assert (status == string_length);

  *string_ptr = string;
  return string_length;
}


// discards everything but the content length; returns -1 on parse failure (i.e.
// bad requests), 0 if content length field was not present, content length
// otherwise.
int read_http_request_header_fields (int http_client_socket)
{
  int read_http_header_line (int, int*, char**);
  int parse_http_field_line (char*, char**, char**);

  char* field_line;
  char* field_name;
  char* field_value;
  int status, content_length = 0;

  while (1)
  {
    status = read_http_header_line (http_client_socket, NULL, &field_line);
    if (status == -1)
    {
      fprintf (stderr, "[error] failed to read an http header line: %s\n", strerror (errno));
      exit (1);
    }

    status = parse_http_field_line (field_line, &field_name, &field_value);
    if (status == -1)
    {
      free (field_line);
      return -1;
    }
    if (status == 1)
    {
      printf ("[EOH]\n");
      free (field_line);
      break;
    }

    printf ("[field name] '%s'\n", field_name);
    printf ("[field_value] '%s'\n\n", field_value);

    if (strcasecmp (field_name, "Content-length") == 0)
    {
      char* remainder;

      errno = 0;
      content_length = strtol (field_value, &remainder, 0);
      if (errno == ERANGE)
        //TODO
        fprintf (stderr, "[error] ERANGE while reading content length\n");
    }

    free (field_line);
  }

  return content_length;
}


int send_http_ok_response (int http_client_socket, int content_length, const char* content)
{
  #define HTTP_OK_RESPONSE_HEADER_FORMAT \
  "HTTP/1.1 200 OK\r\n"\
  "Connection: close\r\n"\
  "Content-type: text/html\r\n"\
  "Content-length: %i\r\n"\
  "Access-Control-Allow-Origin: *\r\n"\
  "\r\n"

  char* response_header;
  int response_header_length;
  int format_to_string (char**, const char*, ...);

  response_header_length = format_to_string (&response_header, HTTP_OK_RESPONSE_HEADER_FORMAT, 14 + content_length);

  write_chars (http_client_socket, response_header_length, response_header);

  write_chars (http_client_socket, 6, "<pre>\n");

  write_chars (http_client_socket, content_length, content);

  write_chars (http_client_socket, 8, "\n</pre>\n");

  free (response_header);

  return 0;
}


int send_http_bad_request_response (int http_client_socket, const char* error_message)
{
  #define HTTP_BAD_REQUEST_RESPONSE_FORMAT \
  "HTTP/1.1 400 BAD REQUEST\r\n"\
  "Connection: close\r\n"\
  "Content-type: text/html\r\n"\
  "Content-length: %i\r\n"\
  "Access-Control-Allow-Origin: *\r\n"\
  "\r\n"\
  "%s\n"

  int status;
  char* response;
  int response_length;

  response_length = format_to_string (&response, HTTP_BAD_REQUEST_RESPONSE_FORMAT, 1 + strlen (error_message), error_message);
  if (response_length == -1)
    return -1;

  status = write_chars (http_client_socket, response_length, response);
  free (response);
  if (status == -1)
    return -1;

  return 0;
}


int send_http_not_implemented_response (int http_client_socket, const char* error_message)
{
  #define HTTP_NOT_IMPLEMENTED_RESPONSE_FORMAT \
  "HTTP/1.1 501 Method Not Implemented\r\n"\
  "Connection: close\r\n"\
  "Content-type: text/html\r\n"\
  "Content-length: %i\r\n"\
  "Access-Control-Allow-Origin: *\r\n"\
  "\r\n"\
  "%s\n"

  int status;
  char* response;
  int response_length;

  response_length = format_to_string (&response, HTTP_NOT_IMPLEMENTED_RESPONSE_FORMAT, 1 + strlen (error_message), error_message);
  if (response_length == -1)
    return -1;

  status = write_chars (http_client_socket, response_length, response);
  free (response);
  if (status == -1)
    return -1;

  return 0;
}


int read_http_header_line (int http_client_socket, int* line_length, char** line)
{
  int read_until (int, const char*, int, int*, char**, int);

  return read_until (2, "\r\n", http_client_socket, line_length, line, 32);
}


// returns -1 on empty lines
//TODO check for other illegal lines
int parse_http_request_line (char* request_line, char** method, char** target, char** version)
{
  int i;

  printf ("[parse req line] input: '%s'\n", request_line);

  if (*request_line == 0)
    return -1;

  if (method != NULL)
    *method = request_line;

  for (i = 0; request_line[i] != ' ' && request_line[i] != 0; i++);
  if (request_line[i] == 0)
    return -1;

  request_line[i] = 0;
  i++;
  if (target != NULL)
    *target = request_line + i;

  for (; request_line[i] != ' ' && request_line[i] != 0; i++);
  if (request_line[i] == 0)
    return -1;

  request_line[i] = 0;
  i++;
  if (version != NULL)
    *version = request_line + i;

  return 0;
}


// returns 1 on empty lines, -1 on illegal lines
//TODO check for other illegal lines
int parse_http_field_line (char* field_line, char** field_name, char** field_value)
{
  int i;

  if (*field_line == 0)
    return 1;

  if (field_name != NULL)
    *field_name = field_line;

  for (i = 0; field_line[i] != ':' && field_line[i] != 0; i++);
  if (field_line[i] == 0)
    return -1;

  field_line[i] = 0;
  i++;

  //TODO replace isspace with appropriate func: see
  //https://www.w3.org/Protocols/rfc2616/rfc2616-sec2.html
  for (; isspace (field_line[i]) && field_line[i] != 0; i++);
  if (field_line[i] == 0)
    return -1;

  if (field_value != NULL)
    *field_value = field_line + i;

  return 0;
}


// read until given terminator byte seq. is encountered (which is discarded).
// NOTE: 0 terminator char IS appended (overwrites first char of discarded
// terminator). read_count_ptr will contain the number of characters read
// EXCLUDING THE TERMINATOR. If buffer_ptr is NULL, everything is discarded; if
// read_count_ptr is NULL, it is not written to. On malloc or read failures
// (including 0 return value from read, i.e. EOF), return -1; if any bytes were
// read, their count is returned in *read_count_ptr and they are returned
// (allocated with malloc) in *buffer_ptr (WITHOUT the 0 terminator); errno will
// contain the error of the failed call.
int read_until (int terminator_length, const char* terminator, int fd, int* read_count_ptr, char** buffer_ptr, int initial_buffer_capacity)
{
  int status;
  int buffer_capacity = initial_buffer_capacity;
  char* buffer;
  int read_count;

  assert (terminator_length >= 1);

  buffer = malloc (buffer_capacity);
  if (buffer == NULL)
    goto failure;

  read_count = 0;
  while (1)
  {
    if (buffer_capacity <= read_count)
    {
      buffer_capacity *= 2;
      buffer = realloc (buffer, buffer_capacity);
      if (buffer == NULL)
        goto failure;
    }

    status = read (fd, buffer + read_count, 1);
    if (status == -1 || status == 0)
      goto failure;

    read_count++;

    if (read_count >= terminator_length)
    {
      for (int i = 0; i <= read_count - terminator_length; i++)
        if (strncmp (buffer + i, terminator, terminator_length) == 0)
        {
          if (buffer_ptr == NULL)
          {
            free (buffer);
            return 0;
          }

          if (read_count_ptr != NULL)
            *read_count_ptr = i;
          buffer[i] = 0;
          *buffer_ptr = buffer;
          return 0;
        }
    }
  }

failure:
  if (buffer_ptr != NULL)
    *buffer_ptr = buffer;
  else if (buffer != NULL)
    free (buffer);

  if (read_count_ptr != NULL)
    *read_count_ptr = read_count;

  return -1;
}


int open_http_daemon_socket (uint16_t port)
{
  int status;
  int daemon_socket;
  struct sockaddr_in addr;

  daemon_socket = socket (PF_INET, SOCK_STREAM, 0);
  if (daemon_socket == -1)
  {
    fprintf (stderr, "[wsh-httpd] failed to open socket: %s\n", strerror (errno));
    exit (1);
  }

  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  addr.sin_port = htons (port);
  status = bind (daemon_socket, (struct sockaddr*)&addr, sizeof addr);
  if (status == -1)
  {
    fprintf (stderr, "[wsh-httpd] failed to bind socket: %s\n", strerror (errno));
    exit (1);
  }

  status = listen (daemon_socket, 3);
  if (status == -1)
  {
    fprintf (stderr, "[wsh-httpd] failed to listen on socket\n");
    exit (1);
  }

  return daemon_socket;
}


int connect_to_shell_server (const char* ip_address, uint16_t port)
{
  int status;
  struct sockaddr_in server_address;
  int shell_server_socket;

  shell_server_socket = socket (PF_INET, SOCK_STREAM, 0);
  if (shell_server_socket == -1)
  {
    fprintf (stderr, "[cli] failed to open socket\n");
    exit (1);
  }

  server_address.sin_addr.s_addr = inet_addr (ip_address);
  if (server_address.sin_addr.s_addr == (in_addr_t)-1)
  {
    fprintf (stderr, "[cli] invalid ip addr\n");
    exit (1);
  }

  server_address.sin_family      = AF_INET;
  server_address.sin_port        = htons (port);

  status = connect ( shell_server_socket
                   , (struct sockaddr*)&server_address
                   , sizeof server_address
                   );
  if (status == -1)
  {
   fprintf (stderr, "[cli] failed to open socket\n");
   exit (1);
  }

  return shell_server_socket;
}


int recv_and_process_msg
  (int shell_server_socket, int http_client_socket, int* is_reading_command_line)
{
  void send_command_line_response (char*);

  static char* accumulated_output = NULL;
  static int accumulated_output_length = 0;

  int status, retval = http_client_socket;
  uint32_t msg_type;
  uint32_t msg_length;
  char*    msg_payload;

  status = recv_msg (shell_server_socket, &msg_type, &msg_length, &msg_payload);
  if (status < 0)
  {
    fprintf (stderr, "[cli] failed to recv message\n");
    exit (1);
  }

  switch (msg_type)
  {
    case MSG_EXIT:
      exit (*msg_payload);
      break;

    case MSG_COMMAND_LINE_REQUEST:
      if (*is_reading_command_line)
      {
        fprintf (stderr, "[cli] unexpected command line request\n");
        exit (1);
      }
      *is_reading_command_line = 1;

      if (http_client_socket != -1)
      {
        send_http_ok_response (http_client_socket, accumulated_output_length, accumulated_output);
        free (accumulated_output);
        accumulated_output = NULL;
        accumulated_output_length = 0;
        close (http_client_socket);
        retval = -1;
      }

      break;

    case MSG_STDOUT:
    case MSG_STDERR:
      status = write_chars (msg_type == MSG_STDOUT ? STDOUT_FILENO : STDERR_FILENO, msg_length, msg_payload);
      if (http_client_socket != -1)
      {
        accumulated_output = realloc (accumulated_output, msg_length + accumulated_output_length);
        memcpy (accumulated_output + accumulated_output_length, msg_payload, msg_length);
        accumulated_output_length += msg_length;
      }

      break;

    default:
      fprintf (stderr, "[cli] unknown message type\n");
      exit (1);
  }

  free (msg_payload);

  return retval;
}


int accept_client (int daemon_socket)
{
  struct sockaddr_in cli_addr;
  unsigned           cli_addr_sz = sizeof cli_addr;
  int client_socket;

  do client_socket =
    accept (daemon_socket, (struct sockaddr*)&cli_addr, &cli_addr_sz);
  while (client_socket == -1 && errno == EINTR);
  if (client_socket == -1)
  {
    fprintf (stderr, "[wsh-httpd] failed to listen on socket\n");
    exit (1);
  }

  return client_socket;
}
