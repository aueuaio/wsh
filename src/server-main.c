// Make sigaction & co. and pselect available.
#define _POSIX_C_SOURCE 200809L

#include <stdio.h> // printf
#include <stdlib.h> // malloc, free, exit
#include <string.h> // strlen, strcpy
#include <sys/types.h> // waipid, kill, connect, socket, pid_t
#include <sys/wait.h> // waipid
#include <errno.h> // errno
#include <signal.h> // sigaction, struct sigaction, sigemptyset, SIG*, kill
#include <sys/select.h> // pselect, FD_*
#include <fcntl.h> // fcntl, FD_CLOEXEC
#include <assert.h> // assert
#include <stdint.h> // uint16_t
#include <sys/socket.h> // connect, socket, AF_INET, SOCK_STREAM, PF_INET
#include <netinet/in.h> // sockaddr_in
//#include <wordexp.h> // wordexp, wordexp_t

#include <security/pam_appl.h> // pam_start, pam_end, pam_authenticate, pam_acct_mgmt, PAM_*, struct pam_conv, struct pam_message, struct pam_response

#include "utils/fd_io.h" // write_chars, set_blocking_flag
#include "parser.h" // parse_command_line
#include "commands_execution.h" // get_builtin_index, execute_builtin, fork_command_pipeline
#include "msg_io.h" // enum msg_type, recv_msg, send_msg



// signal handler to collect the exit status of dead children processes
static void kill_zombies_d (int signum)
{
  int pid;
  int status;
  int old_errno;

  old_errno = errno;

  while (1)
  {
    pid = waitpid (-1, &status, WNOHANG);
    if (pid == -1)
    {
      if (errno == ECHILD)
        break;
      else
      {
        //TODO error msg
        break;
      }
    }
    if (pid == 0)
      continue;
    if (WIFEXITED (status))
      printf ( "wshd: child server process with PID %i terminated "
               "normally with exit status %i\n"
             , pid, WEXITSTATUS (status)
             );
    else if (WIFSIGNALED (status))
    {
      printf ( "wshd: child server process with PID %i terminated due to"
               " a "
               "signal: %s", pid, strsignal (WTERMSIG (status))
             );
      #ifdef WCOREDUMP
      if (WCOREDUMP (status))
        printf (" (core dumped)");
      #endif
      printf ("\n");
    }
  }

  errno = old_errno;
}


static int client_socket;

int server_main (uint32_t);

int main (int argc, char* argv[])
{
  struct sigaction kill_zombies_sa;
  struct sigaction old_sa;
  int              outcome;
  int              daemon_port = 9000;
  int              daemon_sockd;

  kill_zombies_sa.sa_handler = kill_zombies_d;
  kill_zombies_sa.sa_flags   = 0;
  sigemptyset (&kill_zombies_sa.sa_mask);

  // Parse cli args
  {
    char* port_str;
    char* leftover;
    int   port;

    if (argc != 2)
    {
      fprintf(stderr, "wrong number of args (1 expected)\n");
      exit (1);
    }

    port_str = argv[1];

    errno = 0;
    port = strtol (port_str, &leftover, 0);
    if (errno == ERANGE || *leftover != 0 || (port > 65535 || port < 0))
    {
      fprintf (stderr, "the port argument does not represent a valid port number\n");
      exit (1);
    }
    daemon_port = port;
  }

  outcome = sigaction (SIGCHLD, &kill_zombies_sa, &old_sa);
  assert (outcome == 0);

  /* Open 'daemon_sockd', bind it to localhost and start listening for
     connections. */
  {
    struct sockaddr_in addr;

    daemon_sockd = socket (PF_INET, SOCK_STREAM, 0);
    if (daemon_sockd == -1)
    {
      fprintf (stderr, "[dem] failed to open socket\n");
      exit (1);
    }
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons (daemon_port);
    outcome = bind (daemon_sockd, (struct sockaddr*)&addr, sizeof addr);
    if (outcome == -1)
    {
      fprintf (stderr, "[dem] failed to bind socket\n");
      exit (1);
    }

    outcome = listen (daemon_sockd, 3);
    if (outcome == -1)
    {
      fprintf (stderr, "[dem] failed to listen on socket\n");
      exit (1);
    }
  }

  printf ("[dem] successfully started listening for connections\n");

  while (1)
  {
    pid_t child_pid;
    struct sockaddr_in cli_addr;
    unsigned           cli_addr_sz = sizeof cli_addr;

    {
      do client_socket =
        accept (daemon_sockd, (struct sockaddr*)&cli_addr, &cli_addr_sz);
      while (client_socket == -1 && errno == EINTR);
      if (client_socket == -1)
      {
        fprintf (stderr, "[dem] failed to accept a client\n");
        continue;
      }
    }

    child_pid = fork ();
    if (child_pid == 0)
    {
      outcome = close (daemon_sockd);
      outcome = sigaction (SIGCHLD, &old_sa, NULL);
      assert (outcome == 0);

      server_main (cli_addr.sin_addr.s_addr);
    }
    else if (child_pid > 0)
    {
      printf ("[dem] client accepted and served by child server process"
              " with PID %i\n", child_pid);

      outcome = close (client_socket);
    }
    else
    {
      fprintf (stderr, "[dem] failed to fork server child\n");
      exit (1);
    }
  }

  assert ("this assertion indicates an unreachable execution path" == NULL);
}



// These globals are implicitely used by signal handlers, 'print_for_builtins'
// and 'main'; other uses are explicit (i.e. the address or the value of these
// variables is passed as an argument).

// This is:
//
// - NULL while there is no foreground command running or while there is a
//   builtin foreground command running;
//
// - Pointing to a malloc'ed array of pids (terminated by -1) while an external
//   foreground command is running; each time a process of a foreground command
//   terminates, its pid is set to 0 in this array
static pid_t* foreground_command_pids = NULL;


static void terminate_session_coop (int client_socket, char exit_status)
{
  int status;

  status = send_msg (client_socket, MSG_EXIT, 1, &exit_status);
  if (status < 0)
  {
    fprintf (stderr, "failed to send exit message to client\n");
  }

  exit (exit_status);
}


static void print_child_exit_status (pid_t command_pid, int status)
{
  printf ("[shell] ");
  if (WIFEXITED (status))
  {
    printf ( "child process %i terminated normally with exit status %i"
           , command_pid, WEXITSTATUS (status));
  }
  else if (WIFSIGNALED (status))
  {
    printf ( "child process %i terminated due to a signal: %i"
           , command_pid, WTERMSIG (status)
           );
  }
  #ifdef WCOREDUMP
  if (WCOREDUMP (status))
    printf (" (core dumped)");
  #endif // WCOREDUMP
  printf ("\n");
}


// IMPORTANT NOTE: signal handlers here use functions that are NOT
// async-signal-safe. This does not cause problems as long as THESE SIGNALS ARE
// ALWAYS BLOCKED OUTSIDE OF THE PSELECT CALL IN THE MAIN LOOP (even inside
// signal handlers themselves).

static void kill_zombies (int signum)
{
  int pid;
  int wstatus, status;
  int old_errno;

  old_errno = errno;

  while (1)
  {
    pid = waitpid (-1, &wstatus, WNOHANG);
    if ((pid == -1 && errno == ECHILD) || pid == 0)
      break;
    if (pid == -1)
    {
      perror ("[shell] waipid");
      break;
    }

    if (foreground_command_pids != NULL)
    {
      for (int i = 0; foreground_command_pids[i] != -1; i++)
      {
        if (foreground_command_pids[i] == pid)
        {
          foreground_command_pids[i] = 0;
          printf ("[srv] child %i part of fg command terminated\n", pid);
        }
      }

      int is_foreground_command_terminated = 1;

      for (int i = 0; foreground_command_pids[i] != -1; i++)
        if (foreground_command_pids[i] != 0)
          is_foreground_command_terminated = 0;

      if (is_foreground_command_terminated)
      {
        printf ("[srv] current fg command terminated\n");

        free (foreground_command_pids);
        foreground_command_pids = NULL;

        status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
        if (status < 0)
        {
          fprintf (stderr, "failed to request command line\n");
          exit (1);
        }
      }
    }

    print_child_exit_status (pid, wstatus);
  }

  errno = old_errno;
}


static int connect_to_client (uint16_t port, uint32_t* ip_address)
{
  int status;
  int daemon_socket;
  struct sockaddr_in addr;

  daemon_socket = socket (PF_INET, SOCK_STREAM, 0);
  if (daemon_socket == -1)
  {
    fprintf (stderr, "[srv] failed to open socket\n");
    exit (1);
  }

  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  addr.sin_port = htons (port);
  status = bind (daemon_socket, (struct sockaddr*)&addr, sizeof addr);
  if (status == -1)
  {
    fprintf (stderr, "[srv] failed to bind socket\n");
    exit (1);
  }

  status = listen (daemon_socket, 3);
  if (status == -1)
  {
    fprintf (stderr, "[srv] failed to listen on socket\n");
    exit (1);
  }

  struct sockaddr_in cli_addr;
  unsigned           cli_addr_sz = sizeof cli_addr;
  int client_socket;

  do client_socket =
    accept (daemon_socket, (struct sockaddr*)&cli_addr, &cli_addr_sz);
  while (client_socket == -1 && errno == EINTR);
  if (client_socket == -1)
  {
    fprintf (stderr, "[srv] failed to listen on socket\n");
    exit (1);
  }

  *ip_address = cli_addr.sin_addr.s_addr;
  return client_socket;
}


// from the gnu libc manual
/* Set the FD_CLOEXEC flag of desc if value is nonzero,
   or clear the flag if value is 0.
   Return 0 on success, or -1 on error with errno set. */
static int
set_cloexec_flag (int desc, int value)
{
  int oldflags = fcntl (desc, F_GETFD, 0);
  /* If reading the flags failed, return error indication now. */
  if (oldflags < 0)
    return oldflags;
  /* Set just the flag we want to set. */
  if (value != 0)
    oldflags |= FD_CLOEXEC;
  else
    oldflags &= ~FD_CLOEXEC;
  /* Store modified flag word in the descriptor. */
  return fcntl (desc, F_SETFD, oldflags);
}


static int stdout_for_builtins = -1;
static int stderr_for_builtins = -1;

static void print_for_builtins (int is_err_msg, const char* string)
{
  int status;

  //TODO that 1 is a bug? sends the 0-term?
  if ((!is_err_msg && stdout_for_builtins == -1)
      || (is_err_msg && stderr_for_builtins == -1) )
  {
    status = send_msg (client_socket, is_err_msg ? MSG_STDERR : MSG_STDOUT, 1 + strlen (string), string);
    if (status == -1)
      fprintf(stderr, "builtin: could not print and forward to client\n");
    return;
  }

  status = write_chars ( is_err_msg ? stderr_for_builtins : stdout_for_builtins, strlen (string), string);
  if (status == -1)
  {
    fprintf(stderr, "builtin: could not print to redirected file\n");
  }
}


// NOTE: this function should be kept fast to avoid unresponsiveness (e.g. it
// should NOT wait for foreground commands to finish). This function assumes
// there is no foreground command running.
static void process_command_line ( char* command_line
                                 , pid_t** foreground_command_pids
                                 , int client_socket
                                 , int stdin
                                 , int stdout
                                 , int stderr_
                                 , int background_commands_stdin
                                 )
{
  int         status;
  int         command_process_count;
  int*        command_argcs = NULL;
  char***     command_argvs = NULL;
  char*       command_stdin_filename;
  char*       command_stdout_filename;
  char*       command_stderr_filename;
  int         command_stdin, command_stdout, command_stderr;
  int         command_builtin_index;
  int         is_command_foreground;
  const char* parse_error_message;
  //wordexp_t   word_vector;

  assert (*foreground_command_pids == NULL);

  /*status = wordexp (command_line, &word_vector, 0);
  for (int i = 0; i < word_vector.we_wordc; i++)
    printf ("[wordexp] %s\n", word_vector.we_wordv[i]);*/

  status =
    parse_command_line ( command_line
                       , &is_command_foreground
                       , &command_process_count
                       , &command_argcs
                       , &command_argvs
                       , &command_stdin_filename
                       , &command_stdout_filename
                       , &command_stderr_filename
                       , &parse_error_message
                       );
  if (status == -1)
    terminate_session_coop (client_socket, 1);
  if (status == -2)
  {
    char newline_char = '\n';

    printf ("[parser] %s\n", parse_error_message);

    status = send_msg (client_socket, MSG_STDERR, strlen (parse_error_message), parse_error_message);
    if (status < 0)
    {
      fprintf (stderr, "failed to send parse error message\n");
      exit (1);
    }

    status = send_msg (client_socket, MSG_STDERR, 1, &newline_char);
    if (status < 0)
    {
      fprintf (stderr, "failed to send parse error message\n");
      exit (1);
    }

    status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
    if (status < 0)
    {
      fprintf (stderr, "failed to request command line\n");
      exit (1);
    }

    return;
  }

  if (command_stdin_filename != NULL)
  {
    printf ("[parser] stdin filename: %s\n", command_stdin_filename);

    command_stdin = open (command_stdin_filename, O_RDONLY, 0644);
    free (command_stdin_filename);

    if (command_stdin == -1)
    {
      fprintf (stderr, "[srv] unable to redirect stdin: open: %s\n", strerror (errno));

      status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
      if (status < 0)
      {
        fprintf (stderr, "failed to request command line\n");
        exit (1);
      }

      free (command_stdout_filename);
      free (command_stderr_filename);
      goto free_argcs_argvs_and_return;
    }
  }
  else
    command_stdin = is_command_foreground
                      ? stdin
                      : background_commands_stdin;

  if (command_stdout_filename != NULL)
  {
    printf ("[parser] stdout filename: %s\n", command_stdout_filename);

    command_stdout = open (command_stdout_filename, O_WRONLY|O_CREAT, 0644);
    free (command_stdout_filename);

    if (command_stdout == -1)
    {
      fprintf (stderr, "[srv] unable to redirect stdout: open: %s\n", strerror (errno));

      close (command_stdin);

      status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
      if (status < 0)
      {
        fprintf (stderr, "failed to request command line\n");
        exit (1);
      }

      free (command_stderr_filename);
      goto free_argcs_argvs_and_return;
    }

    stdout_for_builtins = command_stdout;
  }
  else
    command_stdout = stdout;

  if (command_stderr_filename != NULL)
  {
    printf ("[parser] stderr filename: %s\n", command_stderr_filename);

    command_stderr = open (command_stderr_filename, O_WRONLY|O_CREAT, 0644);
    free (command_stderr_filename);

    if (command_stderr == -1)
    {
      fprintf (stderr, "[srv] unable to redirect stderr: open: %s\n", strerror (errno));

      close (command_stdin);
      close (command_stdout);

      status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
      if (status < 0)
      {
        fprintf (stderr, "failed to request command line\n");
        exit (1);
      }

      goto free_argcs_argvs_and_return;
    }

    stderr_for_builtins = command_stderr;
  }
  else
    command_stderr = stderr_;

  // A command can be considered builtin only if no pipe operators are present.
  if (command_process_count == 1)
  {
    command_builtin_index = get_builtin_index (command_argvs[0][0]);
  }
  else
    command_builtin_index = -1;

  if (command_builtin_index == -1) // i.e. command is external
  {
    if (is_command_foreground)
    {
      // This must be free'd and set to NULL by the SIGCHLD handler when all
      // processes in this command terminate.
      *foreground_command_pids = malloc ((1+command_process_count) * sizeof **foreground_command_pids);
      if (foreground_command_pids == NULL)
        terminate_session_coop (client_socket, 1);

      (*foreground_command_pids)[command_process_count] = -1;
    }

    status = fork_command_pipeline ( command_stdin
                                   , command_stdout
                                   , command_stderr
                                   , command_process_count
                                   , command_argvs
                                   , *foreground_command_pids
                                   );
    if (status != 0)
    {
      fprintf (stderr, "[srv] failed to fork command pipeline: %i\n", status);
      terminate_session_coop (client_socket, 1);
    }
    if (!is_command_foreground)
    {
      status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
      if (status < 0)
      {
        fprintf (stderr, "failed to request command line\n");
        exit (1);
      }
    }
  }
  else
  {
    status = execute_builtin ( command_builtin_index
                             , print_for_builtins
                             , command_argcs[0]
                             , command_argvs[0]
                             );
    if (status == -1)
    {
      fprintf (stderr, "[srv] failed to execute builtin command\n");
      terminate_session_coop (client_socket, 1);
    }
    else
    {
      printf ("[srv] builtin command terminated with exit status %i\n", status);
    }

    stdout_for_builtins = stderr_for_builtins = -1;

    if (command_builtin_index == exit_builtin_index)
    {
      terminate_session_coop (client_socket, 0);
    }

    status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
    if (status < 0)
    {
      fprintf (stderr, "failed to request command line\n");
      exit (1);
    }
  }

  if (command_stdin_filename != NULL)
    close (command_stdin);
  if (command_stdout_filename != NULL)
    close (command_stdout);
  if (command_stderr_filename != NULL)
    close (command_stderr);

free_argcs_argvs_and_return:
  for (int i = 0; command_argvs[i] != NULL; i++)
  {
    for (int j = 0; j < command_argcs[i]; j++)
      free (command_argvs[i][j]);
    free (command_argvs[i]);
  }
  free (command_argvs);
  free (command_argcs);

  return;
}


// This requests a string from the user of the client with echoing either on or
// off. It is used in pam_conversation_function to ask the client to get
// password etc. from the user.
static char* prompt_client_user (int is_echoing_on, int client_socket, const char* prompt)
{
  int status;
  uint32_t msg_type;
  uint32_t msg_length;
  char* msg_payload;

  status = send_msg ( client_socket
                    , is_echoing_on ? MSG_PAM_PROMPT_ECHO_ON : MSG_PAM_PROMPT_ECHO_OFF
                    , 1 + strlen (prompt)
                    , prompt
                    );
  if (status < 0)
  {
    fprintf (stderr, "[srv] failed to prompt client user\n");
    return NULL;
  }

  status = recv_msg (client_socket, &msg_type, &msg_length, &msg_payload);
  if (status < 0)
  {
    fprintf (stderr, "[cli] failed to recv message\n");
    return NULL;
  }

  if (msg_type != MSG_PAM_PROMPT_RESPONSE)
  {
    fprintf (stderr, "[srv] wrong message type\n");
    free (msg_payload);
    return NULL;
  }

  return msg_payload;
}


static int pam_conversation_function ( int num_msg
                                     , const struct pam_message** msg
                                     , struct pam_response** resp
                                     , void* appdata_ptr
                                     )
{
  const struct pam_message* messages = *msg;
  struct pam_response* responses;
  int client_socket = *((int*)appdata_ptr);

  responses = malloc (num_msg * sizeof *responses);
  if (responses == NULL)
  {
    fprintf (stderr, "[pam] no memory for pam responses\n");
    return PAM_SYSTEM_ERR;
  }

  for (int i = 0; i < num_msg; i++)
  {
    switch (messages[i].msg_style)
    {
      case PAM_PROMPT_ECHO_OFF:
        printf ("[pam] prompt with echo off: %s\n", messages[i].msg);
        responses[i].resp =
          prompt_client_user (0, client_socket, messages[i].msg);
        break;

      case PAM_PROMPT_ECHO_ON:
        printf ("[pam] prompt with echo on: %s\n", messages[i].msg);
        responses[i].resp =
          prompt_client_user (1, client_socket, messages[i].msg);
        break;

      case PAM_ERROR_MSG:
        printf ("[pam] error msg: %s\n", messages[i].msg);
        break;

      case PAM_TEXT_INFO:
        printf ("[pam] text info: %s\n", messages[i].msg);
        break;

      default:
        printf ("[pam] unknown msg style\n");
    }
  }

  *resp = responses;

  return PAM_SUCCESS;
}


static char authenticate_client (int client_socket, const char* user)
{
  int status;
  char is_authenticated = 0;
  pam_handle_t* pam_handle = NULL;
  // NOTE: the address of client_socket is only valid inside this function, so
  // all calls to the pam_conversation_function must occur inside this function.
  struct pam_conv pam_conversation
    = {pam_conversation_function, &client_socket};

  status = pam_start ("wsh", user, &pam_conversation, &pam_handle);

  if (status == PAM_SUCCESS)
  {
    printf ("[pam] pam started, authenticating...\n");
    status = pam_authenticate (pam_handle, PAM_SILENT);
  }

  if (status == PAM_SUCCESS)
  {
    printf ("[pam] user authenticated, calling pam_acct_mgmt...\n");
    status = pam_acct_mgmt (pam_handle, PAM_SILENT);
  }

  is_authenticated = status == PAM_SUCCESS;

  printf ("[pam] is_authenticated: %i\n", is_authenticated);

  status = pam_end (pam_handle, status);
  if (status != PAM_SUCCESS)
  {
    fprintf(stderr, "[pam] failed to end PAM session\n");
  }

  return is_authenticated;
}


int server_main (uint32_t client_ip_address)
{
  const uint32_t localhost_ip_address = 2130706433;

  int status;
  char is_client_user_authenticated;
  uint32_t msg_type;
  uint32_t msg_length;
  char*    msg_payload;
  struct sigaction kill_zombies_sa;
  struct sigaction old_sa;
  int stdio_pipes[3][2];
  int is_stdin_pipe_open;
  sigset_t empty_sigset, handled_signals;

  //client_socket = connect_to_client (9000, &client_ip_address);
  printf ("[srv] client ip addr: %u, localhost: %u\n", ntohl (client_ip_address), localhost_ip_address );

  if (ntohl (client_ip_address) == localhost_ip_address)
    is_client_user_authenticated = 1;
  else
    is_client_user_authenticated =
      authenticate_client (client_socket, getlogin ());

  status = send_msg ( client_socket
                    , MSG_PAM_AUTHENTICATION_STATUS
                    , 1
                    , &is_client_user_authenticated
                    );
  if (status < 0)
  {
    fprintf (stderr, "[srv] failed to send auth status to client\n");
    exit (1);
  }

  if (!is_client_user_authenticated)
  {
    fprintf (stderr, "[srv] the client failed to authenticate\n");
    exit (1);
  }

  sigemptyset (&empty_sigset);

  // Block handled signals to ensure signals are only handled right after the
  // 'pselect' call in the main loop (signal handlers would interfere with the
  // main loop logic if handled elsewhere).
  sigemptyset (&handled_signals);
  sigaddset (&handled_signals, SIGCHLD);
  sigprocmask (SIG_BLOCK, &handled_signals, NULL);

  kill_zombies_sa.sa_handler = kill_zombies;
  kill_zombies_sa.sa_flags   = 0;
  sigemptyset (&kill_zombies_sa.sa_mask);

  status = sigaction (SIGCHLD, &kill_zombies_sa, &old_sa);
  assert (status == 0);

  for (int i = 0; i < 3; i++)
  {
    status = pipe (stdio_pipes[i]);
    if (status == -1)
    {
      fprintf (stderr, "[srv] failed to create stdio pipe %i\n", i);
      terminate_session_coop (client_socket, 1);
    }
  }

  is_stdin_pipe_open = 1;

  status = set_cloexec_flag (stdio_pipes[0][1], 1);
  if (status == -1)
  {
    fprintf (stderr, "[srv] failed to set FD_CLOEXEC flag on the stdin pipe\n");
    terminate_session_coop (client_socket, 1);
  }

  status = send_msg (client_socket, MSG_COMMAND_LINE_REQUEST, 0, NULL);
  if (status < 0)
  {
    fprintf (stderr, "failed to request command line\n");
    exit (1);
  }

  while (1)
  {
    fd_set client_socket_and_stdio_pipes;

    FD_ZERO (&client_socket_and_stdio_pipes);
    FD_SET (client_socket, &client_socket_and_stdio_pipes);
    FD_SET (stdio_pipes[1][0], &client_socket_and_stdio_pipes);
    FD_SET (stdio_pipes[2][0], &client_socket_and_stdio_pipes);

    // Atomically unblock all signals for the duration of the call to ensure
    // signals are only actually handled right after the call (and thus they do
    // not interfere with the main loop logic).
    do status = pselect ( FD_SETSIZE
                        , &client_socket_and_stdio_pipes
                        , NULL, NULL, NULL
                        , &empty_sigset
                        );
    while (status == -1 && errno == EINTR);
    if (status == -1)
    {
      fprintf (stderr, "[srv] pselect failed\n");
      terminate_session_coop (client_socket, 1);
    }

    if (FD_ISSET (client_socket, &client_socket_and_stdio_pipes))
    {
      status = recv_msg (client_socket, &msg_type, &msg_length, &msg_payload);
      if (status < 0)
      {
        fprintf (stderr, "[srv] failed to recv command line\n");
        exit (1);
      }

      switch (msg_type)
      {
        case MSG_EXIT:
          exit (*msg_payload);
          break;

        case MSG_COMMAND_LINE_RESPONSE:
          if (foreground_command_pids != NULL)
          {
            fprintf (stderr, "[srv] unexpected command line\n");
            exit (1);
          }

          if (!is_stdin_pipe_open)
          {
            status = pipe (stdio_pipes[0]);
            if (status == -1)
            {
              fprintf (stderr, "failed to reopen stdin pipe\n");
              terminate_session_coop (client_socket, 1);
            }

            is_stdin_pipe_open = 1;

            status = set_cloexec_flag (stdio_pipes[0][1], 1);
            if (status == -1)
            {
              fprintf (stderr, "[srv] failed to set FD_CLOEXEC flag on the stdin pipe\n");
              terminate_session_coop (client_socket, 1);
            }
          }

          // NOTE: this function should be fast to avoid unresponsiveness.
          process_command_line ( msg_payload
                               , &foreground_command_pids
                               , client_socket
                               , stdio_pipes[0][0]
                               , stdio_pipes[1][1]
                               , stdio_pipes[2][1]
                               , STDIN_FILENO
                               );
          break;

        case MSG_STDIN:
          printf ("[srv] client sent stdin: ");
          fflush (stdout);
          write_chars (STDOUT_FILENO, msg_length, msg_payload);
          printf ("\n");
          status = write_chars (stdio_pipes[0][1], msg_length, msg_payload);
          if (status == -1)
          {
            fprintf (stderr, "[srv] failed to forward stdin to command\n");
          }
          break;

        case MSG_EOF:
          printf ("[srv] client sent EOF\n");
          //TODO errcheck
          close (stdio_pipes[0][0]);
          close (stdio_pipes[0][1]);
          is_stdin_pipe_open = 0;
          break;

        default:
          fprintf (stderr, "[srv] unknown message type\n");
          exit (1);
      }

      free (msg_payload);
    }

    for (int i = 1; i < 3; i++)
    {
      if (FD_ISSET (stdio_pipes[i][0], &client_socket_and_stdio_pipes))
      {
        const int io_forwarding_buffer_size = 64;
        char forwarding_buffer[io_forwarding_buffer_size];
        int read_count;

        // NOTE: EINTR checking can't be avoided since some unhandled signal may
        // be received during the call.
        do read_count = read ( stdio_pipes[i][0]
                             , forwarding_buffer
                             , io_forwarding_buffer_size - 1
                             );
        while (read_count == -1 && errno == EINTR);

        if (read_count == -1)
        {
          fprintf (stderr, "[srv] failed to read from stdio pipe %i\n", i);
          terminate_session_coop (client_socket, 1);
        }

        status = send_msg ( client_socket
                            , i == 1 ? MSG_STDOUT : MSG_STDERR
                            , read_count
                            , forwarding_buffer
                            );
        if (status < 0)
        {
          fprintf (stderr, "[srv] failed to forward stdout/stderr\n");
          exit (1);
        }

        forwarding_buffer[read_count] = 0;
        printf ("[cmd,%i] %s\n", i, forwarding_buffer);
      }
    }
  }

  assert ("it shouldn't be possible to reach this statement" == NULL);
  return 0;
}
