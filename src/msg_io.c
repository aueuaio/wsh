#include "msg_io.h"

#include <stdlib.h> // malloc
#include <string.h> // memcpy
#include <arpa/inet.h> // hton*, ntoh*

#include "utils/fd_io.h"


int recv_msg
  (int socket, uint32_t* data_type, uint32_t* data_length, char** data)
{
  int status;

  status = read_chars (socket, 4, (char*)data_type);
  if (status < 0)
    return -2;

  *data_type = ntohl (*data_type);

  status = read_chars (socket, 4, (char*)data_length);
  if (status < 0)
    return -2;

  *data_length = ntohl (*data_length);

  *data = malloc (*data_length);
  if (*data == NULL)
    return -1;

  status = read_chars (socket, *data_length, *data);
  if (status < 0)
    return -2;

  return 0;
}


int send_msg
  (int socket, uint32_t data_type, uint32_t data_length, const char* data)
{
  int status;
  int serialized_data_length;
  char* serialized_data;

  serialized_data_length = 4 + 4 + data_length;
  serialized_data = malloc (serialized_data_length);
  if (serialized_data == NULL)
    return -1;

  memcpy (serialized_data + 8, data, data_length);

  data_length = htonl (data_length);
  memcpy (serialized_data + 4, &data_length, 4);

  data_type = htonl (data_type);
  memcpy (serialized_data, &data_type, 4);

  status = write_chars (socket, serialized_data_length, serialized_data);
  free (serialized_data);
  if (status < 0)
    return -2;

  return 0;
}
