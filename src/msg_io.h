// Minimal functions to exchange typed, variable-length data over *nix TCP
// sockets.
#ifndef MSG_IO_H
#define MSG_IO_H

#include <stdint.h> // uint32_t


enum msg_type
  { MSG_EXIT
  , MSG_EOF

  , MSG_INTERACTIVITY

  , MSG_PAM_AUTHENTICATION_STATUS
  , MSG_PAM_PROMPT_RESPONSE
  , MSG_PAM_PROMPT_ECHO_ON
  , MSG_PAM_PROMPT_ECHO_OFF
  , MSG_PAM_ERROR_MSG
  , MSG_PAM_TEXT_INFO

  , MSG_COMMAND_LINE_REQUEST
  , MSG_COMMAND_LINE_RESPONSE

  , MSG_STDIN
  , MSG_STDOUT
  , MSG_STDERR
  };


int recv_msg (int, uint32_t*, uint32_t*, char**);
// Sending 0-length data is valid and the pointer can be any value (NULL is
// suggested).
int send_msg (int, uint32_t,  uint32_t,  const char*);


#endif // MSG_IO_H
